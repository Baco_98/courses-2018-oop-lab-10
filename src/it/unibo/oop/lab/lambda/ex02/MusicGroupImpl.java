package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return this.songs.stream().map(Song::getSongName).sorted();
    }

    @Override
    public Stream<String> albumNames() {
        return this.albums.keySet().stream().map(String::toString);
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    	Set<String> stream = new HashSet<>();
		albums.forEach((x, y) -> {
			if (y == year) {
				stream.add(x);
			}
		});
		return stream.stream();
	}

    @Override
    public int countSongs(final String albumName) {
		return (int) songs.stream().filter(x -> x.getAlbumName().isPresent())
				.filter(x -> x.getAlbumName().get().equals(albumName)).count();
    }

    @Override
	public int countSongsInNoAlbum() {
		return (int) songs.stream().filter(x -> {
			if (x.getAlbumName().isPresent()) {
				return false;
			}
			return true;
		}).count();
    }

    @Override
	public OptionalDouble averageDurationOfSongs(final String albumName) {
		if (songs.stream().filter(x -> x.getAlbumName().isPresent()).map(Song::getAlbumName)
				.noneMatch(x -> x.get().equals(albumName))) {

			return OptionalDouble.empty();
		}

    	return songs.stream().
				filter(x -> x.getAlbumName().isPresent()).
				filter(x -> x.getAlbumName().get().equals(albumName)).
				mapToDouble(Song::getDuration).
				average();
	}

	@Override
	public Optional<String> longestSong() {
		Optional<Song> song = songs.stream().max((a, b) -> {
			if (a.getDuration() > b.getDuration()) {
				return 1;
			} else {
				if (a.getDuration() < b.getDuration()) {
					return -1;
				}
			}
			return 0;
		});

		if (song.isPresent()) {
			return Optional.of(song.get().songName);
		}
		return Optional.empty();
	}
	
    @Override
	public Optional<String> longestAlbum() {
		double maxDuration = 0;
		String max;

		final Map<String, Double> mappina = new HashMap<>();

		songs.stream().forEach(x -> {
			if (x.getAlbumName().isPresent()) {
				if (mappina.containsKey(x.getAlbumName().get())) {
					mappina.replace(x.getAlbumName().get(), mappina.get(x.getAlbumName().get()) + x.duration);
				} else {
					mappina.put(x.getAlbumName().get(), x.duration);
				}
			}
		});
		
		
		return null;
	}

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
